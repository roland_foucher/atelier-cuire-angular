export interface News {
  id: number;
  title: string;
  alert: string;
  comment: string;
  cover: string;
}
