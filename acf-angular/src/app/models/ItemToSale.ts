export interface ItemToSale {
  id: number;
  name: string;
  category: string;
  cover: string;
  allCover: string[];
  price: number;
  quantity: number;
  fullName: string;
  comment: string;
}
