export interface Photo {
  id: number;
  cover: string;
  name: string;
}
