import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllItemComponent } from './shopping-page/components/all-item/all-item.component';

const routes: Routes = [{ path: '', component: AllItemComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
