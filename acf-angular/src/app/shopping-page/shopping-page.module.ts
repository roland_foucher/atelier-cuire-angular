import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AllItemComponent } from './components/all-item/all-item.component';

@NgModule({
  declarations: [AllItemComponent],
  imports: [CommonModule],
  exports: [AllItemComponent],
})
export class shoppingPageModule {}
