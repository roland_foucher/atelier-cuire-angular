import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemToSale } from 'src/app/models/ItemToSale';
import { GetFromServerService } from 'src/app/service/get-from-server.service';

@Component({
  selector: 'app-all-item',
  templateUrl: './all-item.component.html',
  styleUrls: ['./all-item.component.scss'],
})
export class AllItemComponent implements OnInit {
  itemList$!: Observable<ItemToSale[]>;

  constructor(private getFromServer: GetFromServerService) {}

  ngOnInit(): void {
    this.itemList$ = this.getFromServer.getAllItemToSale();
  }
}
