import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemToSale } from '../models/ItemToSale';
import { News } from '../models/News';
import { Photo } from '../models/Photo';

@Injectable({
  providedIn: 'root',
})
export class GetFromServerService {
  private readonly serverURL: string =
    'https://atelier-cuir-fuchats-back.herokuapp.com/api';

  constructor(private http: HttpClient) {}

  getAllItemToSale(): Observable<ItemToSale[]> {
    return this.http.get<ItemToSale[]>(`${this.serverURL}/shoppingList`);
  }

  getAllNews(): Observable<News[]> {
    return this.http.get<News[]>(`${this.serverURL}/news`);
  }

  getAllPhotos(): Observable<Photo[]> {
    return this.http.get<Photo[]>(`${this.serverURL}/photo`);
  }
}
