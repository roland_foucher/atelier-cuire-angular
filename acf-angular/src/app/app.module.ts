import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './shopping-page/components/item/item.component';
import { shoppingPageModule } from './shopping-page/shopping-page.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    shoppingPageModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
